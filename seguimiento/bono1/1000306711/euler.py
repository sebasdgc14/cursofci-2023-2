import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint 

class EDO(object):
    """
    EDO
    """
    def __init__(self,a,b,n,y0,f,punto=False):
        self.a = a
        self.b = b
        self.n=n 
        self.y0 = y0
        self.punto = punto
        self.x=[]
        self.y=[]
        self.f=f

    def h(self):
        if self.punto:
            return (self.punto - self.a) / self.n 
        else:
            return (self.b - self.a) / self.n
    
    def X(self):
        if self.punto:
            self.x=np.arange(self.a,self.punto+self.h(),self.h())
            return self.x
        else:
            self.x=np.arange(self.a,self.b+self.h(),self.h())
            return self.x
    
    def euler(self):
        self.X()
        self.y=[self.y0[1]]        
        for i in range(self.n):
            self.y.append(self.y[i]+self.h()*self.f(self.y[i],self.x[i]))
        return self.x,self.y

    def rungekutta(self):
        self.X()
        self.y=[self.y0[1]]
        for i in range(self.n):
            k1 = self.f(self.y[i], self.x[i])
            k2 = self.f(self.y[i] + k1 * self.h()/2, self.x[i] + self.h()/2)
            k3 = self.f(self.y[i] + k2 * self.h() /2, self.x[i] + self.h()/2)
            k4 = self.f(self.y[i] + k3 * self.h(), self.x[i] + self.h())
            self.y.append(self.y[i] + (self.h() / 6) * (k1 + 2*k2 + 2*k3 + k4))
        return self.x,self.y

    def solanalitica(self):
        t=np.linspace(self.a,self.b,self.n)
        self.y=odeint(self.f,self.y0[1],t)
        return t,self.y
    
    def figplot(self):
        plt.figure(figsize=(10,5))
        xeuler,yeuler=self.euler()
        xrunge,yrunge=self.rungekutta()
        xa,ya=self.solanalitica()
        plt.plot(xeuler,yeuler,"blue",label='Euler')
        plt.plot(xa,ya[:],"green",label='Analítica',linewidth=3)
        plt.plot(xrunge,yrunge[:],"black",label='Runge-Kutta',linewidth=2)
        plt.legend()
        plt.grid()
        plt.show()