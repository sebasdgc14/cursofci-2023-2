from particula import *
#La partícula se aproxima desde el espacio y<0 hacía el campo magnético ubicado en y>o.
#Los ángulos theta y phi determinan la dirección del vector velocidad, según las coordenadas esféricas.
#El campo magnético se definió para y>o. Por lo tanto, el dominio de los ángulos theta y phi es de 0 a pi.

"""
NOTA

4.2

- Buuen trabajo
- No funciona para m=0

"""


if __name__ == "__main__": #Se tomaron parámetros adimensionales para facilitar la visualización.
    m=0#1 
    q=-1
    B=0#1  
    Ek=50
    theta=np.pi/2 #Ángulo que forma la velocidad con el eje z.
    phi=np.pi/3   #Ángulo que forma la proyección de la velocidad en el plano xy con el eje x.
    x0=10 
    y0=-25 #Está posición debe ser negativa, ya que el campo magnético está está definido para y>0.
    z0=1
    tf=50 #Tiempo final
    try:
        p=ParticulaCampo(m,q,B,Ek,theta,phi,x0,y0,z0,tf)
        p.figplot()
    except ArgumentosInvalidos as error:
        print("Error. Redefina los ángulos dentro de los rangos válidos")