from Problema1 import MC
import numpy as np
import sympy as sp
import matplotlib.pyplot as plt

if __name__ == '__main__':
    
    a = 0
    b = 1#np.pi
    c = 1000
    n = 1000
    
    p = sp.symbols('p')
    f = sp.tanh(p/7)#p**2 * sp.cos(p)
    sol = MC(a, b, c, n, f)
    
    A=sol.Integrate()
    B=sol.IntegrateAnalitical()

    def Plot(a, b, c, n, f):
        s=list(range(1,n))
        C=[]
        for i in range(len(s)):
            sol2=MC(a, b, c, s[i], f)
            A=sol2.Integrate()
            B=sol2.IntegrateAnalitical()
            C.append(np.abs(A-B))
        plt.title('Gráfico de convergencia')
        plt.xlabel('Número de iteraciones')
        plt.ylabel('Diferencia entre valor analítico y estimado')
        plt.plot(s,C)
        plt.savefig('Montecarlo/Seguimiento2/Problema1.png')
    
    Plot(a, b, c, n, f)

    print('La solución analítica es: {}'.format(B))                      
    print('La solución por Monte Carlo es: {}'.format(A))
    print('El método de promedio de rectángulos tiene pésima convergencia, pero igual llega a la respuesta')


    
