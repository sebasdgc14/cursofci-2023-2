# import numpy as np
# import matplotlib.pyplot as plt
# from matplotlib.animation import FuncAnimation
# from matplotlib.colors import ListedColormap
# from tqdm import tqdm
# import numba as nb
# import imageio
import clase as I

# @nb.jit(nopython=True)
# def Probabilidad_cambio(matriz,i,j,T,J=1,H=0):
#     k_b = 1#1.38e-23
#     N = len(matriz)
#     spin = matriz[i,j]

#     vecinos = matriz[(i+1) % N, j] + matriz[i, (j+1) % N] + matriz[(i-1) % N, j] + matriz[i, (j-1) % N]
#     E_i = -J*spin*vecinos - H*spin

#     E_f = -E_i
#     D_E = E_f-E_i

#     aleatorio = np.random.random()
#     if D_E < 0 or aleatorio<np.exp(-D_E/(k_b*T)):
#         matriz[i,j] *= -1

if __name__ == '__main__':
    L = 1000
    T = 2
    iteraciones = 30

    Sol = I.Ising(L,T,iteraciones)
    Sol.Simulacion()
# material = np.random.choice([1,-1],size=[L,L])

# Frames = []

# for iter in tqdm(range(iteraciones)):
#     for i in range(L):
#         for j in range(L):
#             Probabilidad_cambio(material,i,j,T,H=0)

#     colors = ['yellow','black' ]  # Azul para 1, Rojo para -1
#     cmap = ListedColormap(colors)

#     fig, ax = plt.subplots()
#     ax.imshow(material,cmap=cmap)
#     plt.title(f'Gráfico {iter+1}')

#     fig.canvas.draw()
#     image = np.array(fig.canvas.renderer.buffer_rgba())
#     Frames.append(image)
#     plt.close()
#     # probabilidades = np.zeros((L,L))

# imageio.mimsave('graficos.gif', Frames,loop=0)