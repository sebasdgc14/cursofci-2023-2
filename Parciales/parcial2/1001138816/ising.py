import numpy as np
import matplotlib.pyplot as plt
from numpy.random import rand
from numba import jit

class IsingModel:
    '''
    Clase que modela el comportamiento de un sistema Ising en dos dimensiones, dando la magnetización y
    el calor especifico en función de la temperatura para diferentes tamaños de la red.	
    Los datos de entrada son:
    L: tamaño de la red
    niter: niter sobre el que se hace la simulación. Ya que es costosa computacionalmente, se recomienda
        dejarlo en 100
    nT: numero de temperaturas sobre el que se hace la simulación. Se recomienda dejarlo en 500.
    
    Y contiene los métodos:
    Ising_MC: método que realiza la simulación del modelo de Ising y calcula los valores de magnetización
        y el calor específico.
    plotIsing: grafica los valores de magnetización y el calor específico en función de la temperatura.
    '''
    def __init__(self, L:list, niter:int =100,nT:int =500):
        self.L = np.array(L)
        self.n = niter
        self.nT = nT 
    
    def Ising_MC(self):
        # se incializan las magnitudes
        inic = lambda N: np.random.randint(2, size=(N,N))-1 #red
        T = np.linspace(0.1, 5, self.nT) # temperatura
        NN = self.L # tamaños de la red
        M,C = np.zeros((self.nT,len(NN))), np.zeros((self.nT,len(NN))) # magnetizacion y calor especifico

        for nn,N in enumerate(NN):
            n1, n2  = 1.0/(self.n*N*N), 1.0/(self.n*self.n*N*N) 
            for tt in range(self.nT):
                E1 = M1 = E2 = M2 = 0
                config = inic(N)
                iT=1.0/T[tt]; iT2=iT*iT; #beta
                
                for i in range(self.n): # se itera hasta llegar al equilibrio        
                    for i in range(N):
                        for j in range(N):
                                a = np.random.randint(0, N)
                                b = np.random.randint(0, N)
                                s =  config[a, b]
                                nb = config[(a+1)%N,b] + config[a,(b+1)%N] + config[(a-1)%N,b] + config[a,(b-1)%N]
                                cost = 2*s*nb
                                if cost < 0:
                                    s *= -1
                                elif rand() < np.exp(-cost*iT):
                                    s *= -1
                                config[a, b] = s        

                for i in range(self.n):
                    for i in range(N):
                         for j in range(N):
                                a = np.random.randint(0, N)
                                b = np.random.randint(0, N)
                                s =  config[a, b]
                                nb = config[(a+1)%N,b] + config[a,(b+1)%N] + config[(a-1)%N,b] + config[a,(b-1)%N]
                                cost = 2*s*nb
                                if cost < 0:
                                    s *= -1
                                elif rand() < np.exp(-cost*iT):
                                    s *= -1
                                config[a, b] = s           
                    
                    # se calculan las energias
                    energy = 0
                    for i in range(len(config)):
                        for j in range(len(config)):
                            S = config[i,j]
                            nb = config[(i+1)%N, j] + config[i,(j+1)%N] + config[(i-1)%N, j] + config[i,(j-1)%N]
                            energy += -nb*S

                    Ene = energy/4.    
                    Mag = np.sum(config)       

                    E1 = E1 + Ene
                    M1 = M1 + Mag
                    M2 = M2 + Mag*Mag 
                    E2 = E2 + Ene*Ene

                M[tt,nn] = n1*M1
                C[tt,nn] = (n1*E2 - n2*E1*E1)*iT2

        return M, C, T
    
    def plotIsing(self):
        M, C, T = self.Ising_MC()
        fig, axs = plt.subplots( nrows=1,ncols=2,figsize=(12,6) )    
        fig.suptitle('Variables del modelo Ising simuladas')
        for i in range(len(self.N)):
            axs[0].scatter(T, abs(M[:,i]), s=50, marker='o',label='L=%d'%(self.L[i]))
        axs[0].set(title='Magnetizacion (M) vs Temperatura (T)')
        axs[0].set_xlabel("T", fontsize=20); 
        axs[0].set_ylabel("M", fontsize=20);   
        for i in range(len(self.N)):
            axs[1].scatter(T, C[:,i], s=50, marker='*',label='L=%d'%(self.L[i]))
        axs[1].set(title='Calor especifico (Cv) vs Temperatura (T)')
        axs[1].set_xlabel("T", fontsize=20); 
        axs[1].set_ylabel("Cv", fontsize=20);     
        axs[0].legend(); axs[1].legend()
        plt.savefig("ising.png")
        plt.show()

