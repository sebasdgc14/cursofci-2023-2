import pandas as pd
import matplotlib.pyplot as plt

class alcohol:

    def __init__(self):
        pass

    def leer(self):
        try:

            df=pd.read_csv("datos_alcohol.txt", delimiter=',')
            return df

        except:
            print("error. La tarea Falló, verifique las entradas")
    
    def mayor_cerveza(self):

        try:
            df=self.leer()

            Pcerveza = df.groupby('continente')['porciones_cerveza'].mean()

            MPcerveza = Pcerveza.idxmax()

            return MPcerveza
        
        except:
            print("error. La tarea Falló, verifique las entradas")

    def vino(self):

        try:
            df=self.leer()

            agrupar_df = df.groupby('continente')['porciones_vino'].sum()
            plt.bar(agrupar_df.index, agrupar_df.values)
            plt.xlabel('Continente')
            plt.ylabel('Porciones de Vino')
            plt.title('Histograma de Porciones de Vino por Continente')
            plt.savefig("vino.png")
        
        except:
            print("error. La tarea Falló, verifique las entradas")
    
    def med_min_max(self):

        try:
            df=self.leer()

            mmm = df.groupby('continente')['porciones_licor'].agg(['mean', 'min', 'max'])
            return mmm
        
        except:
            print("error. La tarea Falló, verifique las entradas")


