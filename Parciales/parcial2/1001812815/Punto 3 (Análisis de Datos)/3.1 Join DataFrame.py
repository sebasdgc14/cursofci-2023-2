import pandas as pd

# Crear DataFrames
df1 = pd.DataFrame(data_1)
df2 = pd.DataFrame(data_2)
df3 = pd.DataFrame(data_3)

# Unir dos DataFrames a lo largo de filas
df_concat_rows = pd.concat([df1, df2], ignore_index=True)

# Unir dos DataFrames a lo largo de columnas
df_concat_cols = pd.concat([df1, df2], axis=1)

# Merge para todos los datos y el tercer DF con el valor id
df_merge_all = pd.merge(df_concat_rows, df3, on='id')

# Merge solo los datos que tienen el mismo 'id'
df_merge_id = pd.merge(df1, df3, on='id')

# Mostrar los resultados
print("Concatenación de filas:")
print(df_concat_rows)
print("\nConcatenación de columnas:")
print(df_concat_cols)
print("\nMerge para todos los datos y el tercer DF con el valor id:")
print(df_merge_all)
print("\nMerge solo los datos que tienen el mismo 'id':")
print(df_merge_id)
